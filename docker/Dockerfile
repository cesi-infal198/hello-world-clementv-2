ARG NODEJS_VERSION=20-alpine

# Set the base image to node
FROM node:$NODEJS_VERSION as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN npm install

# We want the production version
RUN npm run build

# Prepare nginx image
FROM nginx:stable

# Copy complied app from previous container
COPY --from=build /app/build /usr/share/nginx/html

# Copy Nginx configuration
COPY docker/nginx.conf /etc/nginx/conf.d/default.conf

# Expose Nginx
EXPOSE 80

# Run Nginx
CMD ["nginx", "-g", "daemon off;"]

